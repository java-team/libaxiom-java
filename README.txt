======================================================
Apache AXIOM 1.2.8 (January 15, 2009)

http://ws.apache.org/commons/axiom/
------------------------------------------------------

___________________
Documentation
===================
 
Documentation can be found within this release and in the main site.

___________________
Support
===================
 
Any problem with this release can be reported to ws-commons mailing list. 
If you are sending an email to the mailing list make sure to add the [AXIOM] prefix to the subject.

Mailing list subscription:
commons-dev-subscribe@ws.apache.org

Thank you for using AXIOM!

The Apache AXIOM Team.
