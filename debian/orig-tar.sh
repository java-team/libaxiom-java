#!/bin/sh -e

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
DIR=${PACKAGE}-${VERSION}
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz

# clean up the upstream tarball
svn export http://svn.apache.org/repos/asf/webservices/axiom/tags/$2/ $DIR
XZ_OPT=--best tar -c -J -f $TAR $DIR
rm -rf $DIR $3
